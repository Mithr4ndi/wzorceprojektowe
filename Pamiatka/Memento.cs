﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pamiatka
{
    class Memento
    {
       public int HealthPoint { get; set; }
       public string Name { get; set; }
       public string State { get; set; }

        public Memento(string name, int healthPoint, string state)
        {
           Name = name;
           HealthPoint = healthPoint;
           State = state;
        }
    }
}
