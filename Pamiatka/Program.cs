﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pamiatka
{
    class Program
    {
        static void Main(string[] args)
        {
            Caretaker caretaker = new Caretaker();
            Character nexo = new Character("Nexo", 14598);

            nexo.ShowCharacter();
           
            caretaker.addMemento(nexo.CreateSoulstone());
           

            nexo.killCharacter();
            nexo.ShowCharacter();
         

            nexo.Resurrection(caretaker.getMemento());
            nexo.ShowCharacter();

    
            Console.ReadKey();

        }
    }
}
