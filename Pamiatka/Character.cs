﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pamiatka
{
    class Character
    {
       
        private string Name { get; set; }
        private int HealthPoint { get; set; }

        private string state;

        private string State
        {
            get
            {
                if (HealthPoint == 0)
                    return "Dead";

                else
                    return "Alive";
            }

            set { state = value; }
        }


        public Character(string name, int hp)
        {
            Name = name;
            HealthPoint = hp;
        }

        public void killCharacter()
        {
            HealthPoint = 0;
            Console.WriteLine("{0} is dead.\n", Name);
        }

        public Memento CreateSoulstone()
        {
            return new Memento(Name, HealthPoint, State);
        }

        public void Resurrection(Memento memento)
        {
            Name = memento.Name;
            HealthPoint = memento.HealthPoint;
            State = memento.State;
            Console.WriteLine("{0} was ressurected.\n", Name);
        }

        public void ShowCharacter()
        {
            Console.WriteLine(Name + ":\n" + "Health Point: "  + HealthPoint + "\n" + "State: " +  State +"\n");
        }


    }
        
    
}
