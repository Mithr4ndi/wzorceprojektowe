﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodaSzablonowa 
{
    class Paladin : CharacterClass
    {
        private int mana = 200;
        private int spellPower = 120;

        public int Mana
        {
            set { mana = value; }
            get { return mana; }
        }

        public int SpellPower
        {
            set { spellPower = value; }
            get { return spellPower; }
        }

        public override void ActivateAura()
        {
            Console.WriteLine("Before --->SpellPower: " + SpellPower);
            Console.WriteLine("Paladin: Honor and faith! I activate my Crusader Aura!");
            Console.WriteLine("After --->SpellPower: " + (SpellPower + 95) + "\n");
        }

        public override void CastBattleCry()
        {
            Console.WriteLine("Paladin: I am ready to battle!");
            Console.WriteLine("Paladin: GLORY FOR THE ALLIANCE\n");
        }

        public override void CastBuff()
        {
            Console.WriteLine("Before --->Mana: " + Mana);
            Console.WriteLine("Paladin: May the Light be with you! I cast Blessing of Wisdom!");
            Console.WriteLine("After --->Mana: " + (Mana + 140) + "\n");
        }

        public override void DrinkPotion()
        {
            Console.WriteLine("Before --->Strength: " + Strength);
            Console.WriteLine("Paladin: *Drink Holy Potion of Strength* Holy power gives me strength!");
            Console.WriteLine("After --->Strength: " + (Strength + 89) + "\n");
        }

        public override void EatFish()
        {
            Console.WriteLine("Before --->Stamina: " + (Stamina + 150));
            Console.WriteLine("Paladin: *Eat stinky fish* For the Holy shi.. Light, my stomach!");
            Console.WriteLine("After --->Stamina: " + (Stamina - 92) + "\n");
        }

        public override void EquipWeapon()
        {
            Console.WriteLine("Before --->\nStamina: " + Stamina );
            Console.WriteLine("Paladin: *Equip Ashbringer* Let the Light bring justice!");
            Console.WriteLine("After ---> \nStamina: " + (Stamina + 150) + "\n");
        }
    }
}
