﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodaSzablonowa
{
    abstract class CharacterClass
    {
        int stamina = 2500;
        int strength = 350;

        public int Stamina { get { return stamina; } }
        public int Strength { get { return strength; } }

        public abstract void ActivateAura();
        public abstract void EquipWeapon();
        public abstract void CastBuff();
        public abstract void EatFish();
        public abstract void DrinkPotion();
        public abstract void CastBattleCry();

        public void PrepareToTheBattle()
        {
            ActivateAura();
            EquipWeapon();
            CastBuff();
            EatFish();
            DrinkPotion();
            CastBattleCry();
            Console.WriteLine(" ");
        }

    
       

    }
}
