﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodaSzablonowa
{
    class DeathKnight : CharacterClass
    {
        private int runicPower = 0;
        private int hasteRating = 150;

        public int RunicPower
        {
            set { runicPower = value; }
            get { return runicPower; }
        }

        public int HasteRating
        {
            set { hasteRating = value; }
            get { return hasteRating; }
        }

        public override void ActivateAura()
        {
            Console.WriteLine("Before --->HasteRating: " + HasteRating);
            Console.WriteLine("Death Knight: Unholy Power! I activate my Unholy Presence!");
            Console.WriteLine("After ---> HasteRating: " + (HasteRating + 95)  +"\n");
        }

        public override void CastBuff()
        {
            Console.WriteLine("Before --->RunicPower: " + RunicPower);
            Console.WriteLine("Death Knight: Winter is Coming! I use my Horn of Winter!");
            Console.WriteLine("After ---> RunicPower: " + (RunicPower + 60) + "\n");
        }

        public override void DrinkPotion()
        {
            Console.WriteLine("Before --->Strength: " + (Strength + 150));
            Console.WriteLine("Death Knight: *Drink Burbon* I feel sthronger! ...hic! Ups, it's not bottle.");
            Console.WriteLine("After ---> Strength: " + (Strength - 89) + "\n");
        }

        public override void EatFish()
        {
            Console.WriteLine("Before --->Stamina: " + Stamina);
            Console.WriteLine("Death Knight:*Eat stinky fish* Yuck, that fish wasn't fresh!");
            Console.WriteLine("After --->Stamina: " + (Stamina + 70) + "\n");
        }

        public override void EquipWeapon()
        {
            Console.WriteLine("Before --->Strength: " + (Strength));
            Console.WriteLine("Death Knight:*Equip Frostmourne* I'm new Lich King! Get down on your knees, mortals!");
            Console.WriteLine("After --->Strength: " + (Strength + 150) + "\n");
        }

        public override void CastBattleCry()
        {
            Console.WriteLine("Death Knight: I am ready to fight!");
            Console.WriteLine("Death Knight: FROSTMOUNRE IS HUNGRY!\n");
        }
    }
}
