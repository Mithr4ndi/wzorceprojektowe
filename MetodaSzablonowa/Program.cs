﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodaSzablonowa
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Death Knight przygotowuje sie do bitwy:");
            CharacterClass deathKnight = new DeathKnight();
            deathKnight.PrepareToTheBattle();

            Console.WriteLine("------------------------------------------------------------");

            Console.WriteLine("Paladin przygotowuje sie do bitwy:");
            CharacterClass paladin = new Paladin();
            paladin.PrepareToTheBattle();

            Console.ReadKey();
        }
    }
}
