﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class DeathKnight : Character
    {

        public DeathKnight( string charName) : base("Death Knight", charName)
        {
        }

        public override void Notify(string toCharClass, string toCharName)
        {
            Random rand = new Random();
            Console.WriteLine(CharClass + " " + CharName + ", attacks " + toCharClass + " " + toCharName + " stealing " + rand.Next(200, 1000) + " health points.");
        }
    }
}
