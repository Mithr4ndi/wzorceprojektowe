﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class BattleArena : BattleArenaMediator
    {
        private Dictionary<string, Character> characters = new Dictionary<string, Character>();


        public override void RegisterCharacter(Character character)
        {
            if (!characters.ContainsValue(character))
            {
                characters[character.CharName] = character;
            }

            character.BattleArena = this;
            Console.WriteLine(character.CharClass + " " + character.CharName + " dolaczyl\a do pojedynku.");
        }

        public override void SendCommunicate(string charClass, string charName, string toCharClass, string toCharName)
        {
            Character character = characters[charName];

            if (character != null)
            {
                Console.WriteLine();
                Console.WriteLine(charClass + " " + charName + " to " + toCharClass + " " + toCharName);
                character.Notify(toCharClass, toCharName );
            }
        }
    }
}
