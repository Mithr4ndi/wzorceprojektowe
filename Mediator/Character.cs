﻿namespace Mediator
{
  abstract class Character
    {
        protected string charClass;
        protected string charName;

        public string  CharClass
        {
            get { return charClass; }
        }

        public string CharName
        {
            get { return charName; }
        }



        protected BattleArenaMediator battleArena;
    
        public BattleArenaMediator BattleArena
        {
            set { battleArena = value; }
            get { return battleArena; }
        }

        public Character(string charClass, string charName)
        {
            this.charClass = charClass;
            this.charName = charName;
        }

        public abstract void Notify(string toCharClass, string toCharName);
        public void SendCommunicate(string toCharClass, string toCharName)
        {
            battleArena.SendCommunicate(charClass, charName, toCharClass, toCharName);
        }
    }
}