﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    abstract class BattleArenaMediator
    {
       public abstract void RegisterCharacter(Character character);
       public abstract void SendCommunicate(string charClass, string charName, string toCharClass, string toCharName);
    }
}
