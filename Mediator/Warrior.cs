﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Warrior : Character
    {
        public Warrior(string charName) : base("Warrior", charName)
        {
        }

        public override void Notify(string toCharClass, string toCharName)
        {
            Random rand = new Random();
            Console.WriteLine(CharClass + " " + CharName + ", attacks " + toCharClass + " " + toCharName + " dealing " + rand.Next(140,2500) + " damage.");
        }
    }
}
