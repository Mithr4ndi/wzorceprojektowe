﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Priest : Character
    {
        public Priest(string charName) : base("Priest", charName)
        {
        }

        public override void Notify(string toCharClass, string toCharName)
        {
            Random rand = new Random();
            Console.WriteLine(CharClass + " " + CharName + ", heals " + toCharClass + " " + toCharName + " healing " + rand.Next(100, 700) + " health points.");
        }
    }
}
