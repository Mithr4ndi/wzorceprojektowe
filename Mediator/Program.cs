﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            BattleArena battleArena = new BattleArena();

            Character deathKnight = new DeathKnight("Imlerith");
            Character warrior = new Warrior("Nexo");
            Character priest = new Priest("Kaileena");

            battleArena.RegisterCharacter(deathKnight);
            battleArena.RegisterCharacter(warrior);
            battleArena.RegisterCharacter(priest);

            deathKnight.SendCommunicate("Priest", "Kaileena");
            warrior.SendCommunicate("Death Knight", "Imlerith");
            priest.SendCommunicate("Warrior","Nexo");

            Console.ReadKey();

        }
    }
}
